<?php
if (!isset($_SESSION)) {
	session_start();
}

include("connect/config.php");
 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Easy bus trip</title>
		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- font awesome -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<!-- animation -->
		<link href="css/animation.css" rel="stylesheet">
		<!-- datepicker -->
		<link href="css/datepicker.css" rel="stylesheet">
		<!-- selectric -->
		<link href="css/selectric.css" rel="stylesheet">
		<!-- slick Slider -->
		<link href="css/slick.css" rel="stylesheet">
		<!-- Responsive -->
      	<link href="js/responsive-menu/responsive.css" rel="stylesheet">
		<!-- SVG icons -->
		<link href="css/icons.css" rel="stylesheet">
		<!-- prettyphoto -->
		<link href="js/prettyphoto/prettyPhoto.css" rel="stylesheet">
		<!-- svg-creater -->
		<link href="css/svg-creater.css" rel="stylesheet">
		<!-- typography -->
		<link href="css/typography.css" rel="stylesheet">
		<!-- shortcodes -->
		<link href="css/shortcodes.css" rel="stylesheet">
		<!-- widget -->
		<link href="css/widget.css" rel="stylesheet">
		<!-- colors -->
		<link href="css/colors.css" rel="stylesheet">
		<!-- Style sheet -->
		<link href="style.css" rel="stylesheet">
		<!-- Mobile responsive -->
      	<link href="css/responsive.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
	<script type="text/javascript" src="http://gc.kis.v2.scr.kaspersky-labs.com/D5BE577B-AD03-4247-B9E4-248C500E3037/main.js" charset="UTF-8"></script></head>
	<body>
		<div class="pre-loader">
			<div class="pre-loader-img"></div>
		</div>
		<button id="top-btn"><i class="fa fa-angle-up" data-toggle="tooltip" title="click"></i></button>
        <i class='search-icon-close fa fa-times'></i>
        <div class='form-search-popup container'>
            <form class="kf-search-menu">
                <input class='input-search' placeholder='search ....' type='text'>
                <button><i class='fa fa-search'></i></button>
            </form>
        </div>
		<div class="wrapper">
			<!--Header starts version.1-->
			<header class="header">
	            <!--Header top row version.1-->
	            <div class="header_v1_top_row" style="background: #fff;">
	               <!--Header container version.1-->
	               <div class="container">
	                  <!--Header left side version.1-->
	                  <div class="kf_element_left">
	                     
	                  <div class="logo-here">
	                     <h1><a href="index.php"><img src="images/logo_v1.png" alt="44 by 150"></a></h1>
	                  </div>
	                  </div>
	               </div>
	               <!--Header container version.1 ends-->
	            </div>
	         </header>
			<!--Header starts version.1 ends-->

			<!--Travel Banner-->
			<div class="banner-slider">
				<!--Banner slides-->
				<div class="items">
					<div class="banner-img banner-overlay">
						<img src="images/slide2.jpg" alt="banner img 1">
					</div>
					<div class="container banner-caption position-left">
						<div class="title-1 animated">Voyagez en bus à moindre coût</div>
						<!--a href="#" class="btn-normal-1 animated effect2-color-1">see more</a>
						<a href="#" class="btn-normal-1 animated effect2-color-2">Join Now</a-->
					</div>
				</div>
				<!--Banner slides-->
				<div class="items">
					<div class="banner-img banner-overlay">
						<img src="images/slide3.jpg" alt="banner img 1">
					</div>
					<div class="container banner-caption position-center">
						<div class="title-1 animated"><strong>Découvrez le</strong></div>
						<div class="title-1 animated">Voyage d'une vie</div>
						<div class="title-3 animated">Laissez nos experts planifier votre visite privée, sur mesure et sans couture, dans 38 destinations inspirantes.</div>
						<!--a href="#" class="btn-normal-1 animated effect2-color-1">see more</a>
						<a href="#" class="btn-normal-1 animated effect2-color-2">Join Now</a-->
					</div>
				</div>
				
			</div>
			<!--Banner slider ends-->

			<!--Travel Banner ends-->
			<div class="content">
				<!--Travel search engine-->
				<form class="travel_search_engine" action="search.php" method="GET">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="tab-content">
									<div class="kode_write_detail search-img02 tab-pane fade in active" role="tabpanel" id="tab-2">
										<h5 class="title-icon"><span class=""><i class="fa fa-search"></i></span><b>Trouver un trajet</b></h5>
										<div class="booking_content">
											<div class="booking_content">
												<div class="row">
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check in calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<select  class="checkin" name="TypeDeplacement" >
																<option value="Allez simple"> Allez simple</option>
																<option value="Allez retour"> Allez retour</option>
																</select >
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check in calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<select  class="checkin" name="Compagnie" >
																<option value="0"> Compagnie</option>
																<option value="ATT">ATT</option>
																<option value="Confort">Confort</option>
																</select >
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check in calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<input style="padding-left: 15px;" placeholder="" name="nbr_passager" class="checkin" type="number" value="1" >
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check in calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<input style="padding-left: 15px;" placeholder="35 000" name="Prix" class="checkin" type="number" >
																<span class="fa fa-money"></span>
															</div>
														</div>
													</div>
												
												</div>
												<div class="row">
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check in calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<input placeholder="D'où partez-vous ?" id="placeholder_adresse" name="Depart" class="checkin" type="text" >
																<div id="map" style="display:none"></div>
																<span class="fa  fa-map-marker"></span>
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check out calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<input placeholder="Où allez-vous ?" id="placeholder_adresse2" name="Destination" class="checkout" type="text" >
																<div id="map2" style="display:none"></div>
																<span class="fa  fa-map-marker"></span>
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check out calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<input placeholder="Date de départ" name="DateDepart" class="checkout" type="date" >
																<span class="fa fa-calendar"></span>
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-12">
														<!--Travel booking submit-->
														<div class="submit-form">
															<button type="submit" class="btn-normal-1 animated effect2-color-1">Rechercher</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
                                
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--Travel search engine ends-->
			</div>
			<footer class="travel-footer overlay">
				<div class="footer-content">
					<div class="container">
					</div>
				</div>
				<div class="created-by-kodeforest-team">
					<div class="container">
						<span>Copyright 2019 - Tous droits réservés</span>
						<ul class="kf_cradit_cards">
							<li><a href="#"><img src="images/card-1.jpg" alt=""></a></li>
							<li><a href="#"><img src="images/card-2.jpg" alt=""></a></li>
							<li><a href="#"><img src="images/card-3.jpg" alt=""></a></li>
							<li><a href="#"><img src="images/card-4.jpg" alt=""></a></li>
						</ul>
					</div>
				</div>
			</footer>
		</div>
		<!-- jQuery -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYwMmFJdNyZX63kaLngIp682B-C0TtieY&libraries=places&callback=initMap"
        async defer></script>
		<script>
	
          function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
        var map = new google.maps.Map(document.getElementById('map2'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
		
        var input = document.getElementById('placeholder_adresse');
        var input2 = document.getElementById('placeholder_adresse2');


        var autocomplete = new google.maps.places.Autocomplete(input);
        var autocomplete2 = new google.maps.places.Autocomplete(input2);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
		  console.log(place.geometry.location.lat());
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);



        });




      }
	
	</script>
		<script src="js/jquery.js"></script>
		<script src="js/modernizr.custom.js"></script>
		<script src="js/jquery.easing.min.js"></script>
		<!-- bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<!-- datepicker -->
		<script src="js/datepicker.js"></script>
		<!-- selectric -->
		<script src="js/selectric.js"></script>
		<!-- slick.min -->
		<script src="js/slick.js"></script>
		<!-- rateing-stars -->
		<script src="js/rateing-stars.js"></script>
		<!-- filtrable-gallery -->
		<script src="js/filtrable-gallery.js"></script>
		<!-- jquery.drawsvg.min -->
		<script src="js/jquery.drawsvg.min.js"></script>
		<!-- demo -->
		<script src="js/effects/demo.js"></script>
		<!-- demo1 -->
		<script src="js/effects/demo3.js"></script>
		<!-- easing -->
		<script src="js/effects/easings.js"></script>
		<!-- tilt.jquery.min -->
		<script src="js/tilt.jquery.min.js"></script>
		<!-- jquery.zoom.min -->
		<script src="js/jquery.zoom.min.js"></script>
		<!-- jquery.ripples-min -->
		<script src="js/jquery.ripples-min.js"></script>
		<!-- tooltip -->
		<script src="js/tooltip.js"></script>
		<!-- prettyphoto -->
		<script src="js/prettyphoto/prettyphoto.js"></script>
		<!-- Responsive SCRIPTS -->
      	<script src="js/responsive-menu/responsive.jquery.js"></script>
		<!-- custom -->
		<script src="js/custom.js"></script>
	</body>
</html>

