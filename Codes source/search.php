<?php
if (!isset($_SESSION)) {
	session_start();
}

include("connect/config.php");
if(isset($_GET["TypeDeplacement"]) && isset($_GET["Compagnie"]) && isset($_GET["nbr_passager"]) && isset($_GET["Prix"])  && isset($_GET["Depart"]) && isset($_GET["Destination"]) && isset($_GET["DateDepart"]) 
&& !empty($_GET["TypeDeplacement"]) && !empty($_GET["Compagnie"]) && !empty($_GET["nbr_passager"]) && !empty($_GET["Prix"]) && !empty($_GET["Depart"]) && !empty($_GET["Destination"]) && !empty($_GET["DateDepart"])
){
	
		$req_ref='select';
		$champ_ref  ='';
		$table_ref  ='table_bus,table_trajet';
		$condition_ref = 'table_bus.IdBus=table_trajet.IdBus';
		$aggr_ref =' order by IdTrajet asc Limit 0,5';		
		$datasTrajet = build_req($req_ref,$champ_ref,$table_ref,$condition_ref,$aggr_ref);      
		$infosTrajet = $datasTrajet->fetch();
		// toutes les conditons ne sont pas encore prise pour la recherche. j'ai donc fait une requête globale sans la prise en compte des critères de recherche
}
else{
	
		$req_ref='select';
		$champ_ref  ='';
		$table_ref  ='table_bus,table_trajet';
		$condition_ref = 'table_bus.IdBus=table_trajet.IdBus';
		$aggr_ref =' order by IdTrajet asc Limit 0,5';		
		$datasTrajet = build_req($req_ref,$champ_ref,$table_ref,$condition_ref,$aggr_ref);      
		$infosTrajet = $datasTrajet->fetch();
}
 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Easy bus trip</title>
		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- font awesome -->
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<!-- jquery-ui -->
		<link href="js/jquery-ui/jquery-ui.css" rel="stylesheet">
		<!-- animation -->
		<link href="css/animation.css" rel="stylesheet">
		<!-- datepicker -->
		<link href="css/datepicker.css" rel="stylesheet">
		<!-- selectric -->
		<link href="css/selectric.css" rel="stylesheet">
		<!-- slick Slider -->
		<link href="css/slick.css" rel="stylesheet">
		<!-- Responsive -->
      	<link href="js/responsive-menu/responsive.css" rel="stylesheet">
		<!-- SVG icons -->
		<link href="css/icons.css" rel="stylesheet">
		<!-- prettyphoto -->
		<link href="js/prettyphoto/prettyPhoto.css" rel="stylesheet">
		<!-- typography -->
		<link href="css/typography.css" rel="stylesheet">
		<!-- shortcodes -->
		<link href="css/shortcodes.css" rel="stylesheet">
		<!-- widget -->
		<link href="css/widget.css" rel="stylesheet">
		<!-- colors -->
		<link href="css/colors.css" rel="stylesheet">
		<!-- Style sheet -->
		<link href="style.css" rel="stylesheet">
		<!-- Mobile responsive -->
      	<link href="css/responsive.css" rel="stylesheet">
		<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
	<script type="text/javascript" src="http://gc.kis.v2.scr.kaspersky-labs.com/D5BE577B-AD03-4247-B9E4-248C500E3037/main.js" charset="UTF-8"></script></head>
	<style>
		#content .hidden{
		display: none;
		}
		#content #loader{
			display: none;
		}
		
button.js-ride__action__form__btn.reserve {
    background-color: #449ac5;
    color: #fff;
}
.col-xs-12.ttle {
    font-size: 13px;
    color: #4b9ec7;
}
i.fa.fa-long-arrow-right {
    margin: 0px 10px 0px;
    font-size: 15px;
    color: #971508;
}
	</style>
	<body>
		<div class="pre-loader">
			<div class="pre-loader-img"></div>
		</div>
        <i class='search-icon-close fa fa-times'></i>
        <div class='form-search-popup container'>
            <form class="kf-search-menu">
                <input class='input-search' placeholder='search ....' type='text'>
                <button><i class='fa fa-search'></i></button>
            </form>
        </div>
		<div class="wrapper" >
			<!--Header starts version.1-->
			<header class="header">
	            <!--Header top row version.1-->
	            <div class="header_v1_top_row" style="background: #fff;">
	               <!--Header container version.1-->
	               <div class="container">
	                  <!--Header left side version.1-->
	                  <div class="kf_element_left">
	                     
	                  <div class="logo-here">
	                     <h1><a href="index.php"><img src="images/logo_v1.png" alt="44 by 150"></a></h1>
	                  </div>
	                  </div>
	               </div>
	               <!--Header container version.1 ends-->
	            </div>
	         </header>
			<!--Header starts version.1 ends-->
			<!--Travel Banner-->
			<div class="kode_banner">
				<!--Sub banner for inner pages-->
				<div class="sub-banner">
					
			<div class="content" style="margin-top: 200px;">
				<!--Travel search engine-->
				<form class="travel_search_engine" action="search.php" method="GET">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="tab-content">
									<div class="kode_write_detail search-img02 tab-pane fade in active" role="tabpanel" id="tab-2">
										<h5 class="title-icon"><span class=""><i class="fa fa-search"></i></span><b>Trouver un trajet</b></h5>
										<div class="booking_content">
											<div class="booking_content">
												<div class="row">
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check in calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<select  class="checkin" name="TypeDeplacement" >
																<option value="Allez simple" <?php if(isset($_GET["TypeDeplacement"]) && $_GET["TypeDeplacement"]=="Allez simple") echo "selected='selected'"; ?>> Allez simple</option>
																<option value="Allez retour" <?php if(isset($_GET["TypeDeplacement"]) && $_GET["TypeDeplacement"]=="Allez retour") echo "selected='selected'"; ?>> Allez retour</option>
																</select >
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check in calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<select  class="checkin" name="Compagnie" >
																<option value="0"> Compagnie</option>
																<option value="ATT" <?php if(isset($_GET["Compagnie"]) && $_GET["Compagnie"]=="ATT") echo "selected='selected'"; ?>>ATT</option>
																<option value="Confort" <?php if(isset($_GET["Compagnie"]) && $_GET["Compagnie"]=="Confort") echo "selected='selected'"; ?>>Confort</option>
																</select >
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check in calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<input style="padding-left: 15px;" placeholder="" name="nbr_passager" class="checkin" type="number" value="<?php if(isset($_GET["nbr_passager"])) echo $_GET["nbr_passager"]; ?>" >
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check in calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<input style="padding-left: 15px;" placeholder="35 000" name="Prix" class="checkin" type="number" value="<?php if(isset($_GET["Prix"])) echo $_GET["Prix"]; ?>" >
																<span class="fa fa-money"></span>
															</div>
														</div>
													</div>
												
												</div>
												<div class="row">
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check in calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<input placeholder="D'où partez-vous ?" id="placeholder_adresse" name="Depart" class="checkin" type="text" value="<?php if(isset($_GET["Depart"])) echo $_GET["Depart"]; ?>" >
																<div id="map" style="display:none"></div>
																<span class="fa  fa-map-marker"></span>
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check out calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<input placeholder="Où allez-vous ?" id="placeholder_adresse2" name="Destination" class="checkout" type="text" value="<?php if(isset($_GET["Destination"])) echo $_GET["Destination"]; ?>">
																<div id="map2" style="display:none"></div>
																<span class="fa  fa-map-marker"></span>
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-6">
														<!--Travel check out calender-->
														<div class="kode_felid">
															<div class="ralative-icon">
																<input placeholder="Date de départ" name="DateDepart" class="checkout" type="date" value="<?php if(isset($_GET["DateDepart"])) echo $_GET["DateDepart"]; ?>" >
																<span class="fa fa-calendar"></span>
															</div>
														</div>
													</div>
													<div class="col-md-3 col-xs-12 col-sm-12">
														<!--Travel booking submit-->
														<div class="submit-form">
															<button type="submit" class="btn-normal-1 animated effect2-color-1">Rechercher</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
                                
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--Travel search engine ends-->
			</div>
				</div>
				<div class="breadcrumb-blog">
					<ul class="breadcrumb orange-bg">
						<li><a href="#"><?php if(isset($_GET['Depart'])) echo $_GET['Depart'].'  ';?></a></li>
						<li class="active"><a href="#"><?php if(isset($_GET['Destination'])) echo $_GET['Destination'];?></a></li>
					</ul>
				</div>
			</div>
			<!--Travel Banner ends-->
			<div class="content" >
				<!--Travel Blog Detail Content-->
				<section>
					<div class="container">
						<div class="blog-tour-grid full-slider" style="padding: 0px 20px;">
						
							<div class="row">
							
								<div class="b col-xs-12 rides-list currency-eur">
								<div class="col-md-7 col-xs-12 col-sm-6" style="border-right: 1px solid #ccc;min-height: 500px;">
									
									<div id="wrapper">
									<div id="comments">
									<?php 
									if($datasTrajet->rowCount()>0){
									$i = 0;
									do{ 
									$i++;
									?>
									<div id="<?php echo $infosTrajet['IdTrajet']; ?>" class="post">
										<div class="aux-id-direct row">
											<div class="col-xs-12" style="border-bottom: 1px solid #ccc;margin-bottom: 10px;padding-bottom: 10px;">
												<div class="sr-row row ride-available ab-test-ride-flags-0 ">
													<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 time-transfer">
														<div class="row">
															<div class="col-xs-12 ride-stations">
																
																<img OnClick="charge_Detail('<?php echo $infosTrajet['IdTrajet']; ?>');" style="width: 150px;    cursor: pointer;" src="images/<?php echo $infosTrajet['Image']; ?>" alt="">
															</div>
														</div>
													</div>
													<div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
														<div class="row">
															<div class="col-xs-12 ttle">
																<?php echo $infosTrajet['LieuDepart']; ?>
																 <i class="fa fa-long-arrow-right"></i> 
																<?php echo $infosTrajet['Destination']; ?>
															</div>
															<div class="col-sm-6 col-lg-6 visible-sm visible-lg">
																<div class="row">
																	<div class="col-xs-12 transf-num">
																		<span class="vcenter transfer-text"><?php echo $infosTrajet['NbrePlace'].' place(s) disponible(s)'; ?></span>
																	</div>
																</div>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-12 col-lg-6 price-actions">
																<div class="row">
																	<div class="col-xs-12 col-sm-4 col-md-12 col-lg-5 total">
																		<span class="num currency-small-cents"><?php echo $infosTrajet['PrixPlace'].' XOF'; ?></span>
																	</div>
																	<div class="col-xs-12 col-sm-8 col-md-12 col-lg-7 action">
																		<button  OnClick="charge_Detail('<?php echo $infosTrajet['IdTrajet']; ?>');" class="js-ride__action__form__btn reserve"> Voir détail</button><br>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php }while($infosTrajet = $datasTrajet->fetch());
									}
									?>
										<div id="loader" style="display:none;width: 5%;"> <img src="loader.gif" />
										</div>
									</div>
									</div>
									<div class="row">
										<div id="multiselect-error" class="col-xs-12 rides-list" data-group="direct" style="display:none">
											<div class="search-result">
												<div class="search-result__not-found">
												<div class="search-result__not-found__text">
												Malheureusement, aucun trajet n'est proposé sur cet itinéraire à la date sélectionnée. Veuillez modifier la date de votre trajet pour obtenir des résultats.
												</div>
												</div>
											</div>
										</div>
									</div>
									</div>
								<div class="col-md-5 col-xs-12 col-sm-6">
								
									<div id="ChargeDetail">
									</div>
								</div>
								</div>
								<div class="pagination" style="display:none">
									<a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
									<a href="#" class="active">1</a>
									<a href="#">2</a>
									<a href="#">3</a>
									<a href="#">4</a>
									<a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
								</div>
								
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="travel-footer overlay">
				<div class="footer-content">
					<div class="container">
					</div>
				</div>
				<div class="created-by-kodeforest-team">
					<div class="container">
						<span>Copyright 2019 - Tous droits réservés</span>
						<ul class="kf_cradit_cards">
							<li><a href="#"><img src="images/card-1.jpg" alt=""></a></li>
							<li><a href="#"><img src="images/card-2.jpg" alt=""></a></li>
							<li><a href="#"><img src="images/card-3.jpg" alt=""></a></li>
							<li><a href="#"><img src="images/card-4.jpg" alt=""></a></li>
						</ul>
					</div>
				</div>
			</footer>
		</div>
		<!-- jQuery -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYwMmFJdNyZX63kaLngIp682B-C0TtieY&libraries=places&callback=initMap"
        async defer></script>
		<script>

var reqParaAct;
function charge_Detail(num) {


	var url;
	url = 'ChargeDetail.php?id='+num;
	
	reqParaAct=getXMLHttpRequestAct();
	reqParaAct.open("GET", url, true);
	reqParaAct.onreadystatechange = insertParaAct;
	reqParaAct.send(null);
	
	// alert(url);
}

function insertParaAct() {
	if (reqParaAct.readyState == 4) {
		if (reqParaAct.status == 200) {

			document.getElementById("ChargeDetail").innerHTML = reqParaAct.responseText;
		}
	}
}

function getXMLHttpRequestAct()
{
var req = false;

try
{
   req=new XMLHttpRequest();
}

catch(e)
{
   try
   {
      req=new ActiveXObject("Msxml2.XMLHTTP");
   }
   catch (e)
   {
      try
      {
         req = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
         req = false;
      }
   }
}

return req;

}


	</script>
		
		<script src="js/jquery.js"></script>
    <script src="js/script.js"></script>
		<script src="js/modernizr.custom.js"></script>
		<!-- jQuery UI LIB AJAX-->
		<script src="js/jquery-ui/jquery-ui.js"></script>
		<!-- bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<!-- datepicker -->
		<script src="js/datepicker.js"></script>
		<!-- selectric -->
		<script src="js/selectric.js"></script>
		<!-- demo -->
		<script src="js/effects/demo.js"></script>
		<!-- demo1 -->
		<script src="js/effects/demo3.js"></script>
		<!-- easing -->
		<script src="js/effects/easings.js"></script>
		<!-- slick -->
		<script src="js/slick.js"></script>
		<!-- rateing-stars -->
		<script src="js/rateing-stars.js"></script>
		<!-- WEATHER SCRIPTS -->
		<script src="js/weather.js"></script>
		<!-- jquery.ripples-min -->
		<script src="js/jquery.ripples-min.js"></script>
		<!-- prettyphoto -->
		<script src="js/prettyphoto/prettyphoto.js"></script>
		<!-- Responsive SCRIPTS -->
      	<script src="js/responsive-menu/responsive.jquery.js"></script>
		<!-- custom -->
		<script src="js/custom.js"></script>
	</body>
</html>

