-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 29, 2019 at 07:34 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_reservation`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_bus`
--

CREATE TABLE IF NOT EXISTS `table_bus` (
  `IdBus` int(11) NOT NULL AUTO_INCREMENT,
  `Compagnie` varchar(255) NOT NULL,
  `Marque` varchar(255) NOT NULL,
  `NbrePlace` int(11) NOT NULL,
  `Couleur` varchar(50) NOT NULL,
  `NumPlaque` varchar(20) NOT NULL,
  `IdUser` int(11) NOT NULL,
  `DateCreate` date NOT NULL,
  PRIMARY KEY (`IdBus`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `table_bus`
--

INSERT INTO `table_bus` (`IdBus`, `Compagnie`, `Marque`, `NbrePlace`, `Couleur`, `NumPlaque`, `IdUser`, `DateCreate`) VALUES
(1, 'ATT', '-', 30, 'Jaune', 'BF 0155', 1, '2019-11-28'),
(2, 'Confort', '-', 50, 'Blanc', 'Bc 8155', 0, '2019-11-28'),
(3, 'Confort1', '-', 50, 'Blanc', 'Bc 8155', 0, '2019-11-28'),
(4, 'Confort2', '-', 50, 'Blanc', 'Bc 8155', 0, '2019-11-28'),
(5, 'Confort3', '-', 50, 'Blanc', 'Bc 8155', 0, '2019-11-28'),
(6, 'Confort4', '-', 50, 'Blanc', 'Bc 8155', 0, '2019-11-28'),
(7, 'Confort5', '-', 50, 'Blanc', 'Bc 8155', 0, '2019-11-28'),
(8, 'Confort5', '-', 50, 'Blanc', 'Bc 8155', 0, '2019-11-28');

-- --------------------------------------------------------

--
-- Table structure for table `table_trajet`
--

CREATE TABLE IF NOT EXISTS `table_trajet` (
  `IdTrajet` int(11) NOT NULL AUTO_INCREMENT,
  `DateDepart` date NOT NULL,
  `DateRetour` date NOT NULL,
  `HeureDepart` time NOT NULL,
  `HeureRetour` time NOT NULL,
  `LieuDepart` varchar(255) NOT NULL,
  `NbrePlace` int(11) NOT NULL,
  `PrixPlace` int(11) NOT NULL,
  `Destination` varchar(255) NOT NULL,
  `Image` varchar(255) NOT NULL,
  `IdUser` int(11) NOT NULL,
  `IdBus` int(11) NOT NULL,
  `DateCreation` datetime NOT NULL,
  PRIMARY KEY (`IdTrajet`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `table_trajet`
--

INSERT INTO `table_trajet` (`IdTrajet`, `DateDepart`, `DateRetour`, `HeureDepart`, `HeureRetour`, `LieuDepart`, `NbrePlace`, `PrixPlace`, `Destination`, `Image`, `IdUser`, `IdBus`, `DateCreation`) VALUES
(1, '2019-11-29', '2019-11-29', '08:00:00', '12:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville2.jpg', 1, 1, '2019-11-28 00:00:00'),
(2, '2019-11-29', '2019-11-29', '15:30:00', '19:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville1.png', 1, 2, '2019-11-28 00:00:00'),
(3, '2019-12-09', '2019-12-17', '08:00:00', '00:00:00', 'Etoile rouge', 3, 30000, 'Ghana', 'ville3.JPG', 1, 5, '2019-11-28 00:00:00'),
(4, '2019-12-10', '2019-12-11', '06:00:00', '00:00:00', 'Parakou', 3, 40000, 'Ghana', 'ville4.JPG', 1, 3, '2019-11-28 00:00:00'),
(5, '2019-11-29', '2019-11-29', '08:00:00', '12:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville2.jpg', 1, 1, '2019-11-28 00:00:00'),
(6, '2019-11-29', '2019-11-29', '15:30:00', '19:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville1.png', 1, 2, '2019-11-28 00:00:00'),
(7, '2019-12-09', '2019-12-17', '08:00:00', '00:00:00', 'Etoile rouge', 3, 5000, 'Lomé', 'ville1.png', 1, 3, '2019-11-28 00:00:00'),
(8, '2019-11-29', '2019-11-29', '08:00:00', '12:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville2.jpg', 1, 1, '2019-11-28 00:00:00'),
(9, '2019-11-29', '2019-11-29', '15:30:00', '19:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville1.png', 1, 2, '2019-11-28 00:00:00'),
(10, '2019-12-09', '2019-12-17', '08:00:00', '00:00:00', 'Etoile rouge', 3, 5000, 'Lomé', 'ville1.png', 1, 3, '2019-11-28 00:00:00'),
(11, '2019-11-29', '2019-11-29', '08:00:00', '12:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville2.jpg', 1, 1, '2019-11-28 00:00:00'),
(12, '2019-11-29', '2019-11-29', '15:30:00', '19:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville1.png', 1, 2, '2019-11-28 00:00:00'),
(13, '2019-12-09', '2019-12-17', '08:00:00', '00:00:00', 'Etoile rouge', 3, 5000, 'Lomé', 'ville1.png', 1, 3, '2019-11-28 00:00:00'),
(14, '2019-11-29', '2019-11-29', '08:00:00', '12:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville2.jpg', 1, 1, '2019-11-28 00:00:00'),
(15, '2019-11-29', '2019-11-29', '15:30:00', '19:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville1.png', 1, 2, '2019-11-28 00:00:00'),
(16, '2019-12-09', '2019-12-17', '08:00:00', '00:00:00', 'Etoile rouge', 3, 5000, 'Lomé', 'ville1.png', 1, 3, '2019-11-28 00:00:00'),
(17, '2019-11-29', '2019-11-29', '08:00:00', '12:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville2.jpg', 1, 1, '2019-11-28 00:00:00'),
(18, '2019-11-29', '2019-11-29', '15:30:00', '19:00:00', 'Stade De L''Amitié - Général Mathieu KEREKOU, Rue 2651, Cotonou, Benin', 20, 500, 'Abomey Calavi, Benin', 'ville1.png', 1, 2, '2019-11-28 00:00:00'),
(19, '2019-12-09', '2019-12-17', '08:00:00', '00:00:00', 'Etoile rouge', 3, 5000, 'Lomé', 'ville1.png', 1, 3, '2019-11-28 00:00:00'),
(20, '2019-12-10', '2019-12-11', '06:00:00', '00:00:00', 'Parakou', 3, 15000, 'Nigéria', 'ville1.png', 1, 3, '2019-11-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `table_user`
--

CREATE TABLE IF NOT EXISTS `table_user` (
  `IdUser` int(11) NOT NULL AUTO_INCREMENT,
  `NomPrenom` varchar(255) NOT NULL,
  `DateNaissance` date NOT NULL,
  `Sexe` varchar(1) NOT NULL,
  `CodeUser` varchar(20) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Passeword` varchar(255) NOT NULL,
  `DateCreate` datetime NOT NULL,
  PRIMARY KEY (`IdUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
