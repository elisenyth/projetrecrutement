<?php
if (!isset($_SESSION)) {
	session_start();
}

include("connect/config.php");

	if(isset($_GET['id'])){
		$req_ref='select';
		$champ_ref  ='';
		$table_ref  ='table_bus,table_trajet';
		$condition_ref = 'table_bus.IdBus=table_trajet.IdBus AND IdTrajet="'.$_GET['id'].'"';
		$aggr_ref ='';		
		$datasTrajet = build_req($req_ref,$champ_ref,$table_ref,$condition_ref,$aggr_ref);      
		$infosTrajet = $datasTrajet->fetch();
	
		

 ?>
 <style>
 
.col-xs-12.ttle {
    font-size: 13px;
    color: #4b9ec7;
}
span.Tgras {
    font-weight: 600;
}
 </style>
 <div class="row">
	<div class="col-xs-12 ride-stations" style="text-align: center;">
 <h4 style="
    text-align: left;
    font-size: 13px;
    font-weight: 600;
">Détail de l'offre </h4>
<hr>
		<img style="width: 60%;margin-bottom: 15px;" src="images/<?php echo $infosTrajet['Image']; ?>" alt="">
	</div>
	
</div>
<div class="row">
	<div class="col-xs-12 ttle">
		<?php echo $infosTrajet['LieuDepart']; ?>
		 <i class="fa fa-long-arrow-right"></i> 
		<?php echo $infosTrajet['Destination']; ?>
	</div>
	<div class="col-sm-6 col-lg-6 visible-sm visible-lg">
		<div class="row">
			<div class="col-xs-12 transf-num">
				<span class="Tgras">Compagnie: </span><span class="vcenter transfer-text"><?php echo''. $infosTrajet['Compagnie']; ?></span><br>
				<span class="Tgras">Départ: </span><span class="vcenter transfer-text"><?php echo''. date_format(new DateTime($infosTrajet['DateDepart']), 'd-m-Y').' à '.$infosTrajet['HeureDepart']; ?></span><br>
				<span class="vcenter transfer-text"><?php echo $infosTrajet['NbrePlace'].' place(s) disponible(s)'; ?></span>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-12 col-lg-6 price-actions">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-12 col-lg-5 total">
				<span class="num currency-small-cents"><?php echo $infosTrajet['PrixPlace'].' XOF'; ?></span>
			</div>
		</div>
	</div>
	<div class="col-xs-12 ">
				<span class="Tgras">Lieu de départ: </span><span class="vcenter transfer-text"><?php echo $infosTrajet['LieuDepart']; ?></span><br>

	</div>
	<div class="col-xs-12 ">
	<button style="float: right;margin-top: 20px;" 
	class="js-ride__action__form__btn reserve right"> Réserver</button>
	</div><br>

</div>
														
														<?php } ?>