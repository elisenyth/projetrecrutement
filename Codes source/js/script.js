$(window).scroll(function(){
	if($(window).scrollTop() == $(document).height() - $(window).height()){
		// alert("nous sommes à la fin"); 
		$.ajax({
			url: "load.php?last_id=" + $(".post:last").attr('id'),
			beforeSend: function(){$("#loader").show();},
			success: function(html){
				if(html){
					$("#comments").append(html);
					$("#loader").hide();
				}else{
					$("#loader").hide();
				}
			}
		});
	}
});