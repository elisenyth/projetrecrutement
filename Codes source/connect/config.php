<?php
global $connect_bdd;
global $connect_bdd_loc;

$bdd_host ='localhost';
$bdd_base = 'db_reservation';
$bdd_log = 'root';
$bdd_mdp = '';
$bdd_connecteur  = 'mysql:host=';
$bdd_connecteur .= $bdd_host;
$bdd_connecteur .= ';dbname=';
$bdd_connecteur .= $bdd_base;
$bdd_connecteur .= ';charset=utf8'; 
if(!$connect_bdd){
	try	{
		$connect_bdd  = new PDO($bdd_connecteur, $bdd_log, $bdd_mdp);
		$connect_bdd->exec('set session sql_mode = NO_AUTO_CREATE_USER');
	}catch(Exception $e){
		die('Erreur : ' .$e->getMessage());
	}
}	

// ==========================================================================================

function bdd_req($type,$req){
	 global $connect_bdd;
//echo $req; echo $type;
	if($type=='query' AND ($req)){
	$res = $connect_bdd->query($req) or die(print_r($connect_bdd->errorInfo()));
	
	return $res;
	}

	if($type=='exec' AND ($req)){
	$r = $connect_bdd->exec($req); 
//echo """;
	return $r;

	}
	return -1;
}



//=============================================================================================

function build_req($req_name,$champ,$table,$condition, $aggr){
//echo '$req1';
	if($req_name=='select'){
		$req ='SELECT ';

		if($champ) $req .= $champ; else $req .= '*';

		$req .=' FROM ';

		if($table) $req .= $table; else return -1;

		if($condition) {$req .=' WHERE( '; $req .= $condition;$req .= ' )';}

		if($aggr) {$req .= $aggr;}
		$req .= ';';

		return(bdd_req('query',$req));
	}

	if($req_name=='insert'){
	$req ='INSERT INTO ';

		if($table) $req .= $table; else return -1;

		if($champ) {$req .=' VALUES('; $req .= $champ; $req .=')';} else return -1;	

		$req .= ';';
		// echo $req;//die();
		return(bdd_req('exec',$req));

	}

	if($req_name=='delete'){
	$req ='DELETE FROM ';

		if($table) $req .= $table; else return -1;
	if($condition) {$req .=' WHERE '; $req .= $condition;}		
	$req .= ';';	return(bdd_req('exec',$req));
	}

	if($req_name=='update'){
	//echo '$req3';

	$req ='UPDATE ';

		if($table) $req .= $table; else return -1;

		if($champ) {$req .=' SET '; $req .= $champ;} else return -1;	

		if($condition) {$req .=' WHERE '; $req .= $condition;}

		$req .= ';';
//echo $req;
		return(bdd_req('exec',$req));

	}

	return -1;

}

if(!$connect_bdd_loc){
	try	{
		$connect_bdd_loc  = new PDO($bdd_connecteur, $bdd_log, $bdd_mdp);
	}catch(Exception $e){
		die('Erreur : ' .$e->getMessage());
	}
}

function bdd_req_clt($type,$req){
	 global $connect_bdd_loc;
//echo $req; echo $type;
	if($type=='query' AND ($req)){
	$res = $connect_bdd_loc->query($req) or die(print_r($connect_bdd_loc->errorInfo()));
	return $res;
	}

	if($type=='exec' AND ($req)){
	$r = $connect_bdd_loc->exec($req); 
//echo """;
	return $r;

	}
	return -1;
}



//=============================================================================================

function build_req_clt($req_name,$champ,$table,$condition, $aggr){
//echo '$req1';
	if($req_name=='select'){
		$req ='SELECT ';

		if($champ) $req .= $champ; else $req .= '*';

		$req .=' FROM ';

		if($table) $req .= $table; else return -1;

		if($condition) {$req .=' WHERE( '; $req .= $condition;$req .= ' )';}

		if($aggr) {$req .= ' '.$aggr;}
		$req .= ';';//echo $req ;die();
		return(bdd_req_clt('query',$req));
	}
	
	
	if($req_name=='insert'){
	$req ='INSERT INTO ';

		if($table) $req .= $table; else return -1;

		if($champ) {$req .=' VALUES('; $req .= $champ; $req .=')';} else return -1;	

		$req .= ';';

//echo $req;die();
		return(bdd_req_clt('exec',$req));

	}

	if($req_name=='delete'){
	$req ='DELETE FROM ';

		if($table) $req .= $table; else return -1;
	if($condition) {$req .=' WHERE '; $req .= $condition;}		
	$req .= ';';	return(bdd_req_clt('exec',$req));
	}

	if($req_name=='update'){
	//echo '$req3';

	$req ='UPDATE ';

		if($table) $req .= $table; else return -1;

		if($champ) {$req .=' SET '; $req .= $champ;} else return -1;	

		if($condition) {$req .=' WHERE '; $req .= $condition;}

		$req .= ';';
		return(bdd_req_clt('exec',$req));

	}
	
	if($req_name=='count'){
	
	$req ='SELECT * FROM ';
	
	if($table) $req .= $table; else return -1;

	if($condition) {$req .=' WHERE( '; $req .= $condition;$req .= ' )';}
	
	if($aggr) {$req .= ' '.$aggr;}
		$req .= ';';
		
	return(bdd_req_clt('query',$req));

	}
	
	return -1;

}
